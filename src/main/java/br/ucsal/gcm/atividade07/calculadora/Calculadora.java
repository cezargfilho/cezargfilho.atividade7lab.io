package br.ucsal.gcm.atividade07.calculadora;

public class Calculadora {

	public static int calculoFatorial(int num) {
		if (num == 0) {
			return 1;
		}
		return num * calculoFatorial(num - 1);
	}

	public static String VtdNumPrimo(int num) {
		if (num == 0) {
			return "O numero n�o � primo";
		} else {
			for (int j = 2; j < num; j++) {
				if (num % j == 0)
					return "O numero n�o � primo";
			}
			return "O numero � primo";
		}
	}

}
