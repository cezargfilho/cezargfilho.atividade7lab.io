package br.ucsal.gcm.atividade07.calculadora;

import java.util.Scanner;

public class CalculadoraTui {

	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Fatorial de:");
		int valorCalculado = scan.nextInt();
		if (valorCalculado < 0) {
			System.out.print("No existe fatorial para nmeros negativos!");
			System.out.close();
		} else {
			int num = Calculadora.calculoFatorial(valorCalculado);
			System.out.println("O fatorial de " + valorCalculado + "  igual a " + num);
			System.out.println(Calculadora.VtdNumPrimo(valorCalculado));
		}
	}

}
