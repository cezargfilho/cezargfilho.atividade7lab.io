package br.ucsal.gcm.atividade07.calculadoraTest;


import org.junit.Test;

import br.ucsal.gcm.atividade07.calculadora.Calculadora;
import org.junit.Assert;

public class CalculadoraTest {

	@Test
	public void testCalcFatorial() {
		int numInput = 5;
		int numEsperado = 120;
		int numAtual = Calculadora.calculoFatorial(numInput);
		Assert.assertEquals(numEsperado, numAtual);

	}

}
